from django.urls import path
from .views import index, announcement_create, announcement_detail, DeleteAnnouncementView

app_name = "doska"

urlpatterns = [
    path(r'', index, name='index'),
    path('create/', announcement_create, name='announcement_create'),
    path('detail/<int:announcement_id>/', announcement_detail, name="detail"),
    path('delete/<int:announcement_id>/', DeleteAnnouncementView.as_view(), name="delete"),
]
