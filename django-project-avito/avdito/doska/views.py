from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, PasswordResetView
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import DeleteView

from .forms import AnnouncementForm
from .models import Announcement


# Create your views here.


def index(request):
    announcements = Announcement.objects.all()
    context = {
        "header": "Ваши объявления",
        "announcements": announcements,
    }
    return render(request, 'index.html', context)


def advt(request):
    return HttpResponse('Second view return')


@login_required
def announcement_create(request):
    if request.method == 'GET':
        form = AnnouncementForm()
    else:
        form = AnnouncementForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect(post.get_absolute_url())

    context = {
        "header": "Create announcement",
        'form': form
    }
    return render(request, 'anno_create.html', context)


def announcement_detail(request, announcement_id):
    announcement = get_object_or_404(Announcement, id=announcement_id)
    header = f"Announcement: {announcement.title}"

    context = {
        "header": header,
        "announcement": announcement,
    }
    return render(request, 'detail.html', context)


class DeleteAnnouncementView(DeleteView):
    model = Announcement
    pk_url_kwarg = "announcement_id"
    template_name = "anno_delete.html"
    success_url = reverse_lazy('doska:index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['header'] = f"Delete announcement #{self.object.id}"
        context['description'] = "description"
        return context

    # def get_success_url(self):
    #     return reverse('post:index')

    @method_decorator(login_required)
    def post(self, *args, **kwargs):
        object = self.get_object()

        if self.request.user == object.author:
            return super().post(self, *args, **kwargs)
        else:
            raise PermissionDenied("You are not the author of this content")
