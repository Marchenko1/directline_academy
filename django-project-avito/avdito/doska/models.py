from django.db import models
from core.models import CustomUser
from django.urls import reverse


def avatar_announcement_path(instance, filename):
    return f'announcement_{instance.author.id}/avatar/{filename}'


class Announcement(models.Model):
    # Объявление
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    avatar = models.ImageField(upload_to=avatar_announcement_path, default=None)

    def __str__(self):
        return f"Announcement from {self.author.username}"

    def get_absolute_url(self):
        return reverse('doska:detail', args=(self.id,))
