from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Announcement


class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = Announcement
        fields = ['avatar', 'title', 'description']

    def clean_image(self):
        image = self.cleaned_data.get('image')
        if image.size >= 5 * 1024 * 1024:
            raise ValidationError("File is too big!", code='size_error')

        return image
