from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class CustomUser(AbstractUser):
    about = models.TextField(max_length=1000, blank=True)
    avatar = models.ImageField(upload_to="profile")
    phone_number = models.CharField(max_length=11, blank=True, null=True)

    def __str__(self):
        return str(self.username)
