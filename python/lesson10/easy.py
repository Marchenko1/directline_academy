class Car:
    def __init__(self, id, brand, country, year, engine_capacity, price):
        self.id = id
        self.brand = brand
        self.country = country
        self.year = year
        self.engine_capacity = engine_capacity
        self.price = price


class Lorry(Car):
    def __init__(self, id, brand, country, year, engine_capacity, price, weight_limit):
        super().__init__(id, brand, country, year, engine_capacity, price)
        self.weight_limit = weight_limit


class CarDealership:
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.cars = []
        self.lorries = []

    def add_car(self, car):
        self.cars.append(car)

    def add_lorry(self, lorry):
        self.lorries.append(lorry)

    def sell_car(self, id):
        for car in self.cars:
            if car.id == id:
                self.cars.remove(car)
                return car
        return None

    def sell_lorry(self, id):
        for lorry in self.lorries:
            if lorry.id == id:
                self.lorries.remove(lorry)
                return lorry
        return None
