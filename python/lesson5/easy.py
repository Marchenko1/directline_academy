# Все задачи текущего блока решите с помощью генераторов списков!

# Задание-1:
# Дан список, заполненный произвольными целыми числами.
# Получить новый список, элементы которого будут
# квадратами элементов исходного списка
# [1, 2, 4, 0] --> [1, 4, 16, 0]

# Задание-2:
# Даны два списка фруктов.
# Получить список фруктов, присутствующих в обоих исходных списках.

# Задание-3:
# Дан список, заполненный произвольными числами.
# Получить список из элементов исходного, удовлетворяющих следующим условиям:
# + Элемент кратен 3
# + Элемент положительный
# + Элемент не кратен 4

import random


def add_new_list_with_sqtr():
    list = [1, 2, 4, 0]
    list_two = [i * i for i in list]
    print(list_two)


add_new_list_with_sqtr()


def merge_fruit_list():
    fruit_one = ['Апельсин', 'Лимон', 'Мандарин', 'Греипфрукт']
    fruit_two = ['Яблоко', 'Апельсин', 'Груша', 'Мандарин']
    merge_fruit = [fruit for fruit in fruit_one if fruit in fruit_two]
    print(merge_fruit)


merge_fruit_list()


def list_three():
    list = [random.randint(-10, 10) for i in range(20)]
    only_positive = [n for n in list if n >= 0]
    print(only_positive)
    only_positive_divided_into_three = [t for t in only_positive if t % 3 == 0]
    print(only_positive_divided_into_three)
    only_positive_divided_into_three_not_into_four = [b for b in only_positive_divided_into_three if b % 4 != 0]
    print(only_positive_divided_into_three_not_into_four)


list_three()
