# Задача-1: Написать класс для фигуры-треугольника, заданного координатами трех точек.
# Определить методы, позволяющие вычислить: площадь, высоту и периметр фигуры.


# Задача-2: Написать Класс "Равнобочная трапеция", заданной координатами 4-х точек.
# Предусмотреть в классе методы:
# проверка, является ли фигура равнобочной трапецией;
# вычисления: длины сторон, периметр, площадь.


class triangle():

    def __init__(self, x1, y1, x2, y2, x3, y3):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3

    def side_length(self, x1, y1, x2, y2):
        return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5

    def perimeter(self):
        return self.side_length(self.x1, self.y1, self.x2, self.y2) + self.side_length(self.x2, self.y2, self.x3,
                                                                                       self.y3) + self.side_length(
            self.x3, self.y3, self.x1, self.y1)

    def area(self):
        a = self.side_length(self.x1, self.y1, self.x2, self.y2)
        b = self.side_length(self.x2, self.y2, self.x3, self.y3)
        c = self.side_length(self.x3, self.y3, self.x1, self.y1)
        s = (a + b + c) / 2
        return (s * (s - a) * (s - b) * (s - c)) ** 0.5

    def height(self):
        a = self.side_length(self.x1, self.y1, self.x2, self.y2)
        b = self.side_length(self.x2, self.y2, self.x3, self.y3)
        c = self.side_length(self.x3, self.y3, self.x1, self.y1)
        s = (a + b + c) / 2
        return 2 * (s * (s - a) * (s - b) * (s - c)) ** 0.5 / a


triangle = triangle(0, 0, 3, 0, 0, 4)
print(f'Perimeter: {triangle.perimeter()}')
print(f'Area: {triangle.area()}')
print(f'Height: {triangle.height()}')


class Trapezoid:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def is_isosceles(self):
        ab = ((self.b[0] - self.a[0]) ** 2 + (self.b[1] - self.a[1]) ** 2) ** 0.5
        bc = ((self.c[0] - self.b[0]) ** 2 + (self.c[1] - self.b[1]) ** 2) ** 0.5
        cd = ((self.d[0] - self.c[0]) ** 2 + (self.d[1] - self.c[1]) ** 2) ** 0.5
        da = ((self.a[0] - self.d[0]) ** 2 + (self.a[1] - self.d[1]) ** 2) ** 0.5
        if ab == cd or bc == da:
            return True
        else:
            return False

    def sides(self):
        ab = ((self.b[0] - self.a[0]) ** 2 + (self.b[1] - self.a[1]) ** 2) ** 0.5
        bc = ((self.c[0] - self.b[0]) ** 2 + (self.c[1] - self.b[1]) ** 2) ** 0.5
        cd = ((self.d[0] - self.c[0]) ** 2 + (self.d[1] - self.c[1]) ** 2) ** 0.5
        da = ((self.a[0] - self.d[0]) ** 2 + (self.a[1] - self.d[1]) ** 2) ** 0.5
        return ab, bc, cd, da

    def perimeter(self):
        ab, bc, cd, da = self.sides()
        return ab + bc + cd + da

    def area(self):
        ab, bc, cd, da = self.sides()
        h = ((self.d[1] - self.a[1]) + (self.c[1] - self.b[1])) / 2
        return h * (da + bc) / 2


a = (0, 0)
b = (0, 4)
c = (3, 4)
d = (1, 0)
trap = Trapezoid(a, b, c, d)
print(trap.is_isosceles())
print(trap.sides())
print(trap.perimeter())
print(trap.area())
