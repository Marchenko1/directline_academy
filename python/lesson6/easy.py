# Задача-1:
# Следующая программа написана верно, однако содержит места потенциальных ошибок.
# используя конструкцию try добавьте в код обработку соответствующих исключений.
# Пример.
# Исходная программа:
def avg(a, b):
    """Вернуть среднее геометрическое чисел 'a' и 'b'.

    Параметры:jhg
        - a, b (int или float).

    Результат:
        - float.
    """
    return (a * b) ** 0.5


try:
    a = float(input("a = "))
    b = float(input("b = "))
    c = avg(a, b)
    print("Среднее геометрическое = {:.2f}".format(c))
except ValueError:
    print('Вводите только числа')

# ПРИМЕЧАНИЕ: Для решения задач 2-4 необходимо познакомиться с модулями os, sys!
# СМ.: https://pythonworld.ru/moduli/modul-os.html, https://pythonworld.ru/moduli/modul-sys.html

# Задача-2:
# Напишите скрипт, создающий директории dir_1 - dir_9 в папке,
# из которой запущен данный скрипт.
# И второй скрипт, удаляющий эти папки.

import os, shutil


def add_directory():
    for i in range(10):
        os.mkdir(os.getcwd() + f'dir_{i}')


def delete_directory():
    for i in range(10):
        os.rmdir(os.getcwd() + f'dir_{i}')


# Задача-3:
# Напишите скрипт, отображающий папки текущей директории.

def file_on_directory():
    for files in os.walk('.'):
        print(files)


file_on_directory()


# Задача-4:
# Напишите скрипт, создающий копию файла, из которого запущен данный скрипт.


def copy_file():
    shutil.copyfile(os.path.abspath('easy.py'), os.path.join(os.getcwd(), 'easy_copy.py'), follow_symlinks=True)


copy_file()
