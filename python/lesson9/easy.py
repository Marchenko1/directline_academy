class TagFactory:
    def create_tag(self, name):
        return f'<{name}></{name}>'


elements = ['html', 'head', 'body', 'div']

factory = TagFactory()
for el in elements:
    tag = factory.create_tag(el)
    print(tag)
