#!/usr/bin/python3

"""
== Лото ==

Правила игры в лото.

Игра ведется с помощью специальных карточек, на которых отмечены числа,
и фишек (бочонков) с цифрами.

Количество бочонков — 90 штук (с цифрами от 1 до 90).

Каждая карточка содержит 3 строки по 9 клеток. В каждой строке по 5 случайных цифр,
расположенных по возрастанию. Все цифры в карточке уникальны. Пример карточки:

--------------------------
    9 43 62          74 90
 2    27    75 78    82
   41 56 63     76      86
--------------------------

В игре 2 игрока: пользователь и компьютер. Каждому в начале выдается
случайная карточка.

Каждый ход выбирается один случайный бочонок и выводится на экран.
Также выводятся карточка игрока и карточка компьютера.

Пользователю предлагается зачеркнуть цифру на карточке или продолжить.
Если игрок выбрал "зачеркнуть":
	Если цифра есть на карточке - она зачеркивается и игра продолжается.
	Если цифры на карточке нет - игрок проигрывает и игра завершается.
Если игрок выбрал "продолжить":
	Если цифра есть на карточке - игрок проигрывает и игра завершается.
	Если цифры на карточке нет - игра продолжается.

Побеждает тот, кто первый закроет все числа на своей карточке.

Пример одного хода:

Новый бочонок: 70 (осталось 76)
------ Ваша карточка -----
 6  7          49    57 58
   14 26     -    78    85
23 33    38    48    71
--------------------------
-- Карточка компьютера ---
 7 11     - 14    87
      16 49    55 77    88
   15 20     -       76  -
--------------------------
Зачеркнуть цифру? (y/n)

Подсказка: каждый следующий случайный бочонок из мешка удобно получать
с помощью функции-генератора.

Подсказка: для работы с псевдослучайными числами удобно использовать
модуль random: http://docs.python.org/3/library/random.html

"""

import random


class Card:
    def __init__(self):
        self.numbers = []
        self.is_marked = []
        self.generate_numbers()
        self.print_card()

    def generate_numbers(self):
        numbers = random.sample(range(1, 91), 15)
        numbers.sort()
        for i in range(3):
            row = [0] * 9
            for j in range(5):
                index = random.randint(0, 8)
                while row[index] != 0:
                    index = random.randint(0, 8)
                row[index] = numbers.pop(0)
                self.is_marked.append(False)
            self.numbers.append(row)

    def print_card(self):
        print("--------------------------")
        for i in range(3):
            row = "  ".join(str(x).rjust(2) if x != 0 else "  " for x in self.numbers[i])
            print(row)
        print("--------------------------")

    def mark_number(self, number):
        for i in range(3):
            for j in range(9):
                if self.numbers[i][j] == number:
                    self.is_marked[i * 9 + j] = True
                    return True
        return False

    def is_finished(self):
        return all(self.is_marked)


class Game:
    def __init__(self):
        self.cards = [Card(), Card()]
        self.draws = random.sample(range(1, 91), 90)
        self.current_draw = 0

    def play(self):
        while True:
            number = self.draws[self.current_draw]
            self.current_draw += 1
            print("Новый бочонок: ", number)
            for i in range(2):
                print("Карточка игрока ", i + 1)
                self.cards[i].print_card()
                answer = input("Зачеркнуть цифру? (y/n)")
                if answer == "y":
                    if not self.cards[i].mark_number(number):
                        print("Вы проиграли!")
                        return i + 1
                elif answer == "n":
                    if self.cards[i].mark_number(number):
                        print("Вы проиграли!")
                        return i + 1
            if self.cards[0].is_finished():
                return 1
            if self.cards[1].is_finished():
                return 2


if __name__ == "__main__":
    game = Game()
    winner = game.play()
    print("Победил игрок ", winner)
