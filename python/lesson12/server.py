import socket
import threading
import json

HOST = 'localhost'
PORT = 5555
clients = {}


def handle_client(conn, addr):
    while True:
        data = conn.recv(1024).decode()
        if not data:
            break
        message = json.loads(data)
        sender = message['sender']
        content = message['content']
        for client in clients:
            if client != sender:
                client.sendall(data.encode())
    conn.close()
    del clients[addr]


def start_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind((HOST, PORT))
        server.listen()
        print(f'Server is listening on {HOST}:{PORT}')
        while True:
            conn, addr = server.accept()
            print(f'Connected by {addr}')
            clients[addr] = conn
            threading.Thread(target=handle_client, args=(conn, addr)).start()


if __name__ == '__main__':
    start_server()
