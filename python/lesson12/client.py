import socket
import threading
import json

HOST = 'localhost'
PORT = 5555


def send_message(sock, name):
    while True:
        message = input()
        data = json.dumps({'sender': name, 'content': message})
        sock.sendall(data.encode())


def receive_message(sock):
    while True:
        data = sock.recv(1024).decode()
        message = json.loads(data)
        sender = message['sender']
        content = message['content']
        print(f'{sender}: {content}')


def start_client(name):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((HOST, PORT))
        print(f'Connected to {HOST}:{PORT}')
        threading.Thread(target=send_message, args=(sock, name)).start()
        threading.Thread(target=receive_message, args=(sock,)).start()


if __name__ == '__main__':
    name = input('Enter your name: ')
    start_client(name)
