# Задание-1:
# Напишите функцию, переводящую км в мили и выводящую информацию на консоль
# т.е функция ничего не возвращает, а выводит на консоль ответ самостоятельно
# Предполагается, что 1км = 1,609 мили

def convert(km):
    miles = km * 0.621371
    print(miles)

convert(20)

# Задание-2:
# Напишите функцию, округляющую полученное произвольное десятичное число
# до кол-ва знаков (кол-во знаков передается вторым аргументом).
# Округление должно происходить по математическим правилам (0.6 --> 1, 0.4 --> 0).
# Для решения задачи не используйте встроенные функции и функции из модуля math.

def my_round(number, ndigits):
    list_num = []
    for i in str(number):
        list_num.append(i)
    list_split = str(number).split('.')
    list_ost = []
    i = 0
    langth = len(list_split[1])
    if langth <= ndigits:
        print('Нет такой огромной десятичной части у этого числа')
    else:
        for split in list_split[1]:
            list_ost.append(split)
            i += 1
            if i >= ndigits + 1:
                break
        if int(list_ost[ndigits]) >= 5:
            list_ost[ndigits-1] = str(int(list_ost[ndigits-1]) + 1)
        list_ost.pop()
        list_ost.insert(0, f'{list_split[0]}.')

        print(''.join(list_ost))

my_round(2.34534, 4)
# print(my_round(2.1234567, 5))
# print(my_round(2.1999967, 5))
# print(my_round(2.9999967, 5))


# Задание-3:
# Дан шестизначный номер билета. Определить, является ли билет счастливым.
# Решение реализовать в виде функции.
# Билет считается счастливым, если сумма его первых и последних цифр равны.
# !!!P.S.: функция не должна НИЧЕГО print'ить, должна возвращать либо True,
# ибо False (если счастливый и несчастливый соответственно)

def lucky_ticket(ticket_number):
    if len(str(ticket_number)) != 6:
        print("Введен некоректный номер билета, необходимо ровно 6 цифр")
    else:
        ticket_number = str(ticket_number)
        list_number = []

        for i in ticket_number:
            list_number.append(int(i))
        first_sum = list_number[0] + list_number[1] + list_number[2]




print(lucky_ticket(123006))
print(lucky_ticket(12321))
print(lucky_ticket(436751))
