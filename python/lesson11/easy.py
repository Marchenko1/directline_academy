import threading
import time


class Book:
    def __init__(self):
        self.text = ""
        self.readers = 0
        self.writer = threading.Lock()
        self.readers_lock = threading.Lock()

    def write(self, writer_id, text):
        self.writer.acquire()
        print("Writer", writer_id, "start writing")
        # Записываем текст в книгу
        self.text += text
        time.sleep(2)
        print("Writer", writer_id, "finish writing")
        self.writer.release()

    def read(self, reader_id):
        self.readers_lock.acquire()
        self.readers += 1
        # Если это первый читатель, то блокируем запись в книгу
        if self.readers == 1:
            self.writer.acquire()
        self.readers_lock.release()
        print("Reader", reader_id, "start reading")
        # Читаем текст из книги
        time.sleep(1)
        print("Reader", reader_id, "finish reading")
        self.readers_lock.acquire()
        self.readers -= 1
        # Если это был последний читатель, то разблокируем запись в книгу
        if self.readers == 0:
            self.writer.release()
        self.readers_lock.release()


book = Book()


# Функция для писателя
def writer_func(writer_id, text, delay):
    time.sleep(delay)
    book.write(writer_id, text)


# Функция для читателя
def reader_func(reader_id, delay):
    time.sleep(delay)
    book.read(reader_id)


# Создаем потоки для писателей
writer_thread_1 = threading.Thread(target=writer_func, args=(1, "Привет Мир!\n", 0))
writer_thread_2 = threading.Thread(target=writer_func, args=(2, "ДЗ Марченко!\n", 2))
# Создаем потоки для читателей
reader_thread_1 = threading.Thread(target=reader_func, args=(1, 1))
reader_thread_2 = threading.Thread(target=reader_func, args=(2, 3))
reader_thread_3 = threading.Thread(target=reader_func, args=(3, 4))
# Запускаем потоки
writer_thread_1.start()
reader_thread_1.start()
writer_thread_2.start()
reader_thread_2.start()
reader_thread_3.start()
# Ожидаем завершения потоков
writer_thread_1.join()
writer_thread_2.join()
reader_thread_1.join()
reader_thread_2.join()
reader_thread_3.join()
